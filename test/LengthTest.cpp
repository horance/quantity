#include <gtest/gtest.h>
#include "quantity/length/Length.h"

TEST(Length, 1_feet_equal_to_12_inch)
{
    ASSERT_TRUE(Length(1, FEET) == Length(12, INCH));
}

TEST(Length, 1_yard_equal_to_3_feet)
{
    ASSERT_TRUE(Length(1, YARD) == Length(3, FEET));
}

TEST(Length, 1_mile_equal_to_1760_yard)
{
    ASSERT_TRUE(Length(1, MILE) == Length(1760, YARD));
}
