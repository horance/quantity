#include <gtest/gtest.h>
#include "quantity/volume/Volume.h"

TEST(Volume, 1_tbsp_equal_to_3_tsp)
{
    ASSERT_TRUE(Volume(1, TBSP) == Volume(3, TSP));
}

TEST(Volume, 1_oz_equal_to_2_tbsp)
{
    ASSERT_TRUE(Volume(1, OZ) == Volume(2, TBSP));
}
